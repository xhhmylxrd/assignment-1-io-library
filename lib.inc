%define TO_MAKE_CHAR_FROM_INT 0x30
%define DEVIDER 0xA
%define LINE_BREAK 0xA
%define MINUS_ASCII_CODE 0x2D
%define MORE_THAN_SPACES_CODE 0x21
%define SPACE_ASCII_CODE 0x20
%define ZERO_ASCII_CODE 0x30
%define NINE_ASCII_CODE 0x39
%define MULTIPLIER 10
%define TAB_ASCII_CODE 0x9
section .text


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ;в rax будем накапливать длину строки
.loop:
    cmp byte[rax+rdi], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi	
    call string_length ;в rax запишется длина строки
    pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi; клажем код символа в стек	
    mov rsi, rsp ;кладем в rsi адрес кода символа, лежащего в стеке 
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINE_BREAK
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp; rsp - callee-saved
    dec rsp
    xor rax, rax
    mov byte[rsp], al
    mov r9, DEVIDER ;делитель для получения десятичных чисел
    mov rax, rdi
.loop:
    dec rsp
    xor rdx, rdx
    div r9
    add rdx, TO_MAKE_CHAR_FROM_INT
    mov byte[rsp], dl
    cmp rax, 0
    jne .loop
.output:
    mov rdi, rsp
    push r8
    call print_string
    pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns .pr_pos
.pr_neg:
    neg rdi
    push rdi
    mov rdi, MINUS_ASCII_CODE
    call print_char
    pop rdi
.pr_pos:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
.loop:
    mov r9b, byte[r8+rdi]
    cmp r9b, byte[r8 + rsi]
    jne .ret_zero
    cmp r9b, 0
    je .ret_one
    inc r8
    jmp .loop
.ret_zero:
    mov rax, 0
    ret
.ret_one:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, 0 ;номер системного вызова, который соответствует rax 
    mov rdi, 0 ;указываем откуда считывать
    mov rdx, 1 ;указываем количество символов
    mov rsi, rsp ;указываем куда записывать то, что прочитали
    syscall
    cmp rax, 0
    je .return
    mov al, byte[rsp]
.return:
    inc rsp
    ret


; Принимает: адрес начала буфера(rdi), размер буфера(rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor r10, r10
    xor r9, r9
.slovo:
    cmp r10, rsi
    jae .too_long
    push r10
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    pop r10
    cmp al, 0
    je .sovsem_end
    cmp al, SPACE_ASCII_CODE
    je .end
    cmp al, TAB_ASCII_CODE
    je .end
    cmp al, LINE_BREAK
    je .end
    mov r9, 1
    mov byte[r10+rdi], al
    inc r10
    jmp .slovo
.too_long:
    xor rax, rax
    ret
.end:
    cmp r9, 0
    je .slovo
.sovsem_end:
    mov r11, r10
    add r11, rdi
    mov byte[r11], 0
    mov rax, rdi
    mov rdx, r10
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r8, MULTIPLIER
    xor r9, r9
.loop:
    cmp byte[rdi+r9], ZERO_ASCII_CODE
    jb .end
    cmp byte[rdi+r9], NINE_ASCII_CODE
    ja .end
    mul r8
    sub byte[rdi+r9], ZERO_ASCII_CODE
    add al, byte[rdi+r9]
    inc r9
    jmp .loop
.end:
    mov rdx, r9
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], MINUS_ASCII_CODE
    jne .positive
    inc rdi
    call parse_uint
    cmp rdx, 0
    jne .good_end_neg
    ret
.positive: 
    call parse_uint
    cmp rdx, 0
    jne .good_end
    ret
.good_end_neg:
    neg rax
    inc rdx
.good_end:
    ret
.err:
    xor rdx, rdx
    ret


; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    je .bad_end
    mov r8b, byte[rdi+rax]
    mov byte[rsi+rax], r8b
    inc rax
    cmp r8b, 0
    je .end
    jmp .loop
.bad_end:
    mov rax, 0
.end:
    ret

